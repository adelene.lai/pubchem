Location: Chemical and Physical Properties => Experimental Properties
Section: Collision Cross Section
Tooltip: Molecular collision cross section (CCS) values measured following ion mobility separation (IMS)
Read more: https://doi.org/10.1002/mas.21585
