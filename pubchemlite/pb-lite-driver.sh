# Define the following environment variables:
# INPUTDIR : Where are the input files located
# OUTDIR : Where to store the build after its completion
# MF_DIRS: Where to copy the MetFrag results
# TOPDIR: the "build" directory
# TIER_IDC: indices of tiers



# The subdir of the local clone of the pubchem repo where the scripts
# are.
SCRIPTDIR=$(dirname $(readlink -f "$BASH_SOURCE"))

source "${SCRIPTDIR}/library.sh"

[ ! -d "${INPUTDIR}" ] && say -e "(pb-lite-driver): Error. INPUTDIR does not exist: ${INPUTDIR}" && exit -1
[ -z ${OUTDIR+x} ] && say -e "(pb-lite-driver): Error. OUTDIR not defined." && exit -2
[ -z ${MF_DIRS+x} ] && say -e "(pb-lite-driver): Error. MF_DIRS not defined." && exit -3
for mfd in "${MF_DIRS[@]}";do
    [ ! -d "${mfd}" ] && \
	say -e "(pb-lite-driver): Error. MetFrag CSV dir ${mfd} does not exist." && exit -4
done
[ ! -d "${TOPDIR}" ] && say -e "(pb-lite-driver): Error. TOPDIR does not exist: ${TOPDIR}" && exit -5
[ -z "${TIER_IDC+x}" ] && say -e "(pb-lite-driver): Error. TIER_IDC does not exist: ${TIER_IDC[@]}." && exit -5


# Local build dir.
export WORKDIR=$TOPDIR/tiers
mkdir -p "$WORKDIR"

cd "$TOPDIR"

# # Clean up.
# rm -vf "$WORKDIR"/*.{log,out,rpt,csv}

# # Take care of the backup, if there is what to backup.
# [ -d "$OUTDIR" ] && rm -Rf "${OUTDIR}.bak"
# [ -d "$OUTDIR" ] && mv "${OUTDIR}" "${OUTDIR}.bak"


# # Download sources for the build.
# (cd "$WORKDIR"; source "${SCRIPTDIR}/fetch_sources") 2> "${WORKDIR}/fetch_sources.log"
# ! [ $? ] && \
#     echo -e "(pb-lite.sh): Errors during fetch_sources. See ${WORKDIR}/fetch_sources.log" >&2 \
#     && exit -1

# # Download and copy the scripts where needed.
# (cd "$WORKDIR"; source "${SCRIPTDIR}/fetch_scripts") 2> "${WORKDIR}/fetch_scripts.log"
# ! [ $? ] && \
#     echo -e "(pb-lite.sh): Errors during fetch_scripts. See ${WORKDIR}/fetch_scripts.log" >&2 \
#     && exit -2

# # Is there anything obviously crazy with the sources?
# (cd "$WORKDIR"; source "${SCRIPTDIR}/sanity_prebuild") 2> "${WORKDIR}/sanity_prebuild.log"
# ! [ $? ] && \
#     echo -e "(pb-lite.sh): Errors during sanity_prebuild. See ${WORKDIR}/sanity_prebuild.log" >&2 \
#     && exit -3

# # Build tiers.
# ( cd "$WORKDIR"
#   declare -a TIER_IDC=( 0 1 )
#   for num in ${TIER_IDC[@]};do
#       fn="$INPUTDIR/filter_tier${num}.lst"
#       [ ! -e "$fn" ] && echo "Error! Input filter file $fn not found. Aborting." >&2 && exit 1
#       cp "$fn" .
#   done

#   source "$SCRIPTDIR/generate" 2>generate.log 1>generate.out 
# )

# Dump WORKDIR to OUTDIR.
mkdir -p "$OUTDIR"
rsync -auz --log-file="${WORKDIR}/tier.transfer.log"  "${WORKDIR}/" "$OUTDIR"


# Back up the previous tiers on target.
for dr in "${MF_DIRS[@]}"; do
    mkdir -p "$dr/archive"
done

for num in ${TIER_IDC[@]};do
    for dr in "${MF_DIRS[@]}"; do
	fn=${dr}/PubChemLite_tier${num}.csv
	if [ -e "$fn" ]; then
	    date_suff=$(stat --format %y "$fn" |awk '{print $1}'|tr -d '-')
	    spref="${dr}/PubChemLite_tier${num}"
	    tgt="$dr/archive/PubChemLite_tier${num}_${date_suff}.csv"
	    cp -a "${spref}.csv" "$tgt"
	fi
    done
done

# Check CSVs after the build process.
source "$SCRIPTDIR"/sanity_postbuild 2> "${WORKDIR}/sanity_postbuild.log"

# If postbuild checks bad, then exit.
[ ! $? ] && exit -1


# Copy, preserving timestamps if possible.

for dr in "${MF_DIRS[@]}"; do
    cp -a "${WORKDIR}"/*.csv "$dr" 
done
