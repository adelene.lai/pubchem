PDIR=$(dirname "$BASH_SOURCE")
. "$PDIR/param.sh"


function say {
    echo $@ >&2
    }




function fetch {
    
    FN_SRC=$(readlink -f "$1")
    status=$?
    DEST_DIR=$2


    if [ "${status}" -ne 0 ]; then
	
	say -e "Error. The file containing the list of files to be
    downloaded does not exist.  "
	
	exit 1
    fi

    cd "${DEST_DIR}"
    say Downloading source files to "${DEST_DIR}"
    while read fn; do
	# Check for md5 hashes
	bfn=$(basename "$fn")
	mdfn="${bfn}.md5"
	rm -f "${mdfn}"
	say -e "(fetch): Downloading ${fn}.md5"
	! (curl -s -f -R -O "${fn}.md5") &&
	    say -e "(fetch): Warning: MD5 hash for file $fn not found."

	newhash=""
	if [ -e "$mdfn" ]; then
	    newhash=$(awk '{print $1}' "${mdfn}")
	fi
	
	if [ -e "$bfn" ] && [ -e "${mdfn}" ]; then
	    
	    oldhash=$(md5sum "${bfn}"|awk '{print $1}')
	    [[ "$newhash" == "$oldhash" ]] && \
		say -e "(fetch): Skipping unchanged file $fn ." && \
		continue
	else
	    say -e "(fetch): Downloading $fn"
	    ! (curl -s -f -R -O "$fn") && \
		say -e "(fetch): Error: File could not be downloaded from:" $fn ".Aborting" && \
		exit -1

	    dwnhash=$(md5sum "${bfn}"|awk '{print $1}')
	    [[ "$newhash" != "$dwnhash" ]] && \
		say -e "(fetch): Error: Download of $fn corrupted. Abort." && \
		exit -2
	fi
    done < "$FN_SRC"

    exit 0
    # xargs -n1 curl -R -O < $FN_SRC 

}

function fetch_wild {
    url=$1
    patt=$2
    dwn=$3
    wget -o wget.log -nd -P "$dwn" -e robots=off -w 1 --random-wait -l1 -r "$url" -A"$patt"
    say "Downloaded to $dwn"
}

function gen_tier_a {

    cidbits=$1
    filtertier=$2
    tierno=$3
    echo bits : "$cidbits" 1>&2
    echo filter : "$filtertier" 1>&2
    say 'o' stage a: START '(' $(date) ')'
    ( ${DCMPR} < "$cidbits" | "${SCRIPTS[filter_toc_info]}" "$filtertier" 1 | ${CMPR} > "./tier${tierno}_bits.tsv.gz" ) 2> "./tier${tierno}_bits.rpt"
    status=$?
    say 'o' stage a: END '(' $(date) ')'
    return $status
}

function gen_tier_b {
    tierno=$1
    say 'o' stage b: START '(' $(date) ')'
    ( "${SCRIPTS[pull_cid_content]}" "./tier${tierno}_bits.tsv.gz" > "./tier${tierno}_data.out") 2> "./tier${tierno}_data.rpt"
    status=$?
    say 'o' stage b: END '(' $(date) ')'
    return $status
}

function gen_tier_c {
    tierno=$1
    say 'o' stage c: START '(' $(date) ')'
    ( "${SCRIPTS[rest_grab_props]}" < "./tier${tierno}_data.out" | ${CMPR} > "./tier${tierno}_data_complete.out.gz" ) 2> "./tier${tierno}_data_complete.rpt"
    status=$?
    say 'o' stage c: END '(' $(date) ')'
    return $status

}

function gen_tier_d {
    tierno=$1
    say 'o' stage d: START '(' $(date) ')'
    ( ${DCMPR} < "tier${tierno}_data_complete.out.gz" | "${SCRIPTS[remove_unwanted_cases]}" "tier${tierno}_" > "PubChemLite_tier${tierno}.csv" ) 2> "PubChemLite_tier${tierno}.rpt"
    say 'o' stage d: END '(' $(date) ')'
    return $status
}



function gen_tier {
    cidbits=$1
    filtertier=$2
    tierno=$3
    gen_tier_a "$cidbits" "$filtertier" "$tierno" && gen_tier_b "$tierno" && gen_tier_c "$tierno" && gen_tier_d "$tierno"
}
