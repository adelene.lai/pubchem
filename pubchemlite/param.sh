## This is the script "constants" file. Change when architecture
## changes.



NTHREAD=6    # Number of threads for (de)compression.
CMPR="pigz -p ${NTHREAD}"    # Thing to use for compression.
DCMPR="pigz -p ${NTHREAD} --decompress"    # Thing to use for decompression.

## Destination directory.
DDIR=$PWD

# Program auxiliary files.
PDIR=$(dirname "$BASH_SOURCE")

# Destination directory for perl and other scripts.
SCDIR=$DDIR

declare -A SCRIPTS
SCRIPTS=([filter_toc_info]="$SCDIR/filter_toc_info.pl" \
	 [pull_cid_content]="$SCDIR/pull_cid_content.pl" \
	 [rest_grab_props]="$SCDIR/rest_grab_props.pl" \
	 [remove_unwanted_cases]="$SCDIR/remove_unwanted_cases.pl")
