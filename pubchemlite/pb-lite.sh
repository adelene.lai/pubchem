# The build code repository.
PB_REPO="https://git-r3lab.uni.lu/todor.kondic/pubchem.git"

# Location of the filter_tier?.lst input files.
INPUTDIR=/mnt/share/CompoundDBs/PubChem/PubChemLite/input

# Dump of the build.
OUTDIR=/mnt/share/CompoundDBs/PubChem/PubChemLite/tiers

# Metfrag dir.
MF_DIR=/mnt/share/CompoundDBs/MetFrag_CSVs
MF_DIR_01=/mnt/share-01/CompoundDBs/MetFrag_CSVs

# Check if the shares are properly mounted.

! (df -P /mnt/share | tail -1 | awk '{ print $1}'|grep -q LCSB_ECI) && echo "(pb-lite.sh): ERROR: LCSB_ECI not mounted on /mnt/share. Abort." && exit -1

! (df -P /mnt/share-01 | tail -1 | awk '{ print $1}'|grep -q LCSB_ECI_01) && echo "(pb-lite.sh): ERROR: LCSB_ECI_01 not mounted on /mnt/share-01. Abort." && exit -1

# Top dir.
TOPDIR=/mnt/SCRATCH/.wildwest

# Local clone of the build code.
PB_REPO_DIR=$TOPDIR/pubchem

# Where the scripts are located.
SCRIPTDIR="${PB_REPO_DIR}/pubchemlite"

# Local build dir.
WORKDIR=$TOPDIR/tiers
mkdir -p "$WORKDIR"

cd "$TOPDIR"

# Clean up.
rm -vf "$WORKDIR"/*.{log,out,rpt,csv}


# Clone if it wasn't cloned yet.
if [ ! -d "pubchem" ]; then
    git clone "${PB_REPO}"
fi

# Update.
( cd "${PB_REPO_DIR}"; git pull )


# Take care of the backup, if there is what to backup.
[ -d "$OUTDIR" ] && rm -Rf "${OUTDIR}.bak"
[ -d "$OUTDIR" ] && mv "${OUTDIR}" "${OUTDIR}.bak"


# Download sources for the build.
(cd "$WORKDIR"; source "${SCRIPTDIR}/fetch_sources") 2> "${WORKDIR}/fetch_sources.log"
! [ $? ] && \
    echo -e "(pb-lite.sh): Errors during fetch_sources. See ${WORKDIR}/fetch_sources.log" >&2 \
    && exit -1

# Download and copy the scripts where needed.
(cd "$WORKDIR"; source "${SCRIPTDIR}/fetch_scripts") 2> "${WORKDIR}/fetch_scripts.log"
! [ $? ] && \
    echo -e "(pb-lite.sh): Errors during fetch_scripts. See ${WORKDIR}/fetch_scripts.log" >&2 \
    && exit -2

# Is there anything obviously crazy with the sources?
(cd "$WORKDIR"; source "${SCRIPTDIR}/sanity_prebuild") 2> "${WORKDIR}/sanity_prebuild.log"
! [ $? ] && \
    echo -e "(pb-lite.sh): Errors during sanity_prebuild. See ${WORKDIR}/sanity_prebuild.log" >&2 \
    && exit -3

# Build tiers.
( cd "$WORKDIR"
  declare -a tier_idc=( 0 1 )
  for num in ${tier_idc[@]};do
      fn="$INPUTDIR/filter_tier${num}.lst"
      [ ! -e "$fn" ] && echo "Error! Input filter file $fn not found. Aborting." >&2 && exit 1
      cp "$fn" .
  done

  source "$SCRIPTDIR/generate" 2>generate.log 1>generate.out 
)

# Dump WORKDIR to OUTDIR.
mkdir -p "$OUTDIR"
rsync -auz --log-file="${WORKDIR}/tier.transfer.log"  "${WORKDIR}/" "$OUTDIR"


# Back up the previous tiers on target.
mkdir -p "${MF_DIR}/archive"
mkdir -p "${MF_DIR_01}/archive"
declare -a tier_idc=( 0 1 )
for num in ${tier_idc[@]};do
    date_suff=$(stat --format %y "${MF_DIR}/PubChemLite_tier${num}.csv" |awk '{print $1}'|tr -d '-')
    spref="${MF_DIR}/PubChemLite_tier${num}"
    pref1="${MF_DIR_01}/PubChemLite_tier${num}"
    tgt="archive/PubChemLite_tier${num}_${date_suff}.csv"
    cp -a "${spref}.csv" "${MF_DIR}/${tgt}"
    cp -a "${spref}.csv" "${MF_DIR_01}/${tgt}"
done

# Copy, preserving timestamps if possible.
cp -a "${WORKDIR}"/*.csv "${MF_DIR}" 
cp -a "${WORKDIR}"/*.csv "${MF_DIR_01}"
